FROM alpine:3.8 AS stage1
RUN touch /file1

FROM alpine:3.8 as stage2
RUN touch /file2

FROM alpine:3.8 AS stage3
COPY --from=stage1 /file1 /file1
COPY --from=stage2 /file2 /file2

RUN touch /file3

FROM python:3.7-alpine AS stage4
COPY --from=stage1 /file1 /file1
COPY --from=stage2 /file2 /file2
