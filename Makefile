CI_REGISTRY_IMAGE ?= registry.gitlab.com/metachu/buildkit_ci
TAG ?= latest

build:
	@docker run \
    --rm \
    --privileged \
    -v $(shell pwd)/.cache:/cache \
    -v $(shell pwd):/code \
    -v ${HOME}/.docker:/root/.docker\
    --entrypoint buildctl-daemonless.sh \
    moby/buildkit:v0.6.0 \
        build \
        --frontend dockerfile.v0 \
        --local context=/code \
        --local dockerfile=/code \
		--import-cache type=registry,ref=${CI_REGISTRY_IMAGE}:buildcache \
		--export-cache type=registry,ref=${CI_REGISTRY_IMAGE}:buildcache \
        --output type=docker,name=${CI_REGISTRY_IMAGE}:${TAG} | docker load  && docker push ${CI_REGISTRY_IMAGE}:${TAG}

shell:
	docker run \
    --rm \
    --privileged \
    -v $(shell pwd)/.cache:/cache \
    -v $(shell pwd):/tmp/work \
    --entrypoint buildctl-daemonless.sh \
    moby/buildkit:master \
		sh

.PHONY: build shell
